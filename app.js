var express = require('express');
var app = express();
var router = require('./app/Route/web');
var config = require('./app/Config/Config');
var viewNunjucks = require('nunjucks');
var bodyParser = require('body-parser');
var fs = require('fs-extra');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var passport = require("passport");
var passportJWT = require("passport-jwt");
var _ = require('lodash');

var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var jwtOptions = {};

jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
jwtOptions.secretOrKey = 'tasmanianDevil';

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'secret';
opts.issuer = 'accounts.examplesoft.com';
opts.audience = 'yoursite.net';
passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    User.findOne({id: jwt_payload.sub}, function(err, user) {
        if (err) {
            return done(err, false);
        }
        
        if (user) {
            return done(null, user);
        } else {
            return done(null, false);
            // or you could create a new account
        }
    });
}));



require('./app/Cache/Feed');

mongoose.Promise = global.Promise;

// Connect database
var mongoDB = 'mongodb://dev:dev@ds219098.mlab.com:19098/shipdy';

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

fs.readdirSync(__dirname + '/app/Model').forEach(function (file) {
    global[_.upperFirst(_.camelCase(file.replace('.js', '')))] = require(__dirname + '/app/Model/' + file);
});

viewNunjucks.configure('./app/View/', {
    autoescape: true,
    express: app
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(router);

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});

