var express = require('express');
var router = express.Router();

var UserController = require('../Controller/UserController.js');
var FeedController = require('../Controller/FeedController.js');
var AuthController = require('../Controller/AuthController.js');

// Default 
router.get('/', function(req, res) {
	return res.send('Hello world!')
});

// UserController
router.get('/api/user', UserController.fetchAll);
router.post('/api/v1/user/create', UserController.create);

// FeedController
router.post('/api/v1/feed', FeedController.fetchFeeds);
router.post('/api/v1/feed/store', FeedController.postFeed);

// Auth
router.post('/api/v1/login', AuthController.login);

module.exports = router;

