exports.Phones = function (knwl) {
    
    this.languages = {
        'english': true,
    };

    this.areaCodeLength = 3; // Hard code this assumption for now

    // IMPORTANT: This function makes the assumption that there is always 3 digits in an area code
    this.formatPhoneNumber = function(number) {
        var formattedNumber = number.slice(number.length - 7, number.length - 4) + "-" +
            number.slice(number.length - 4, number.length);

        formattedNumber = "(" + number.slice(number.length - (phones.areaCodeLength + 7), number.length - 7) + ") " +
            formattedNumber;

        if (number.length > (phones.areaCodeLength + 7)) {
            formattedNumber = "+" + number.slice(0, number.length - (phones.areaCodeLength + 7)) +
                " " + formattedNumber;
        }
        return formattedNumber;
    };

    this.calls = function() {
        var results = [];

        var words = knwl.words.get('words');
        var currWord = null;
        var phoneRegexp = /^\d{7,13}$/;
        var areaCodeRegExp = /^\d{3}$/;
        var countryCodeRegExp = /^\d{1,3}$/;

        for (var i = 0; i < words.length; i++) {
            currWord = knwl.tasks.removeCharacters(["-", "(", ")"], words[i]);

            if (phoneRegexp.test(currWord)) {
                if (i > 0 && currWord.length === 7) {
                    var areaCode = knwl.tasks.removeCharacters(["(", ")"], words[i - 1]);
                    if (areaCodeRegExp.test(areaCode)) {
                        currWord = areaCode + currWord;
                        if (i > 1) {
                            var countryCode = knwl.tasks.removeCharacters("+", words[i - 2]);
                            if (countryCodeRegExp.test(countryCode)) {
                                currWord = countryCode + currWord;
                            }
                        }
                    }
                } else if (i > 0 && currWord.length === (phones.areaCodeLength + 7)) {
                    var countryCode = knwl.tasks.removeCharacters("+", words[i - 1]);
                    if (countryCodeRegExp.test(countryCode)) {
                        currWord = countryCode + currWord;
                    }
                }

                if (currWord.length >= (7 + phones.areaCodeLength)) {
                    var phoneObj = {
                        phone: phones.formatPhoneNumber(currWord),
                        preview: knwl.tasks.preview(i),
                        found: i
                    };
                    results.push(phoneObj);
                }
            }
        }
        return results;
    };

    var phones = this;
};