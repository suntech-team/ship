var moment = require('moment');
var mongoose = require('mongoose');

var FeedSchema = new mongoose.Schema({
	message: String,
    permalink_url: String,
    from: {
        name: String,
        id: String
    },
    created_time: String,
    updated_time: String,
    id: String,
    phone: String,
    comment_total_count: Number,
    system_created_time: {type: Number, default: moment().utcOffset(420).unix()}
}, {id: false, versionKey: 'v'});

module.exports = mongoose.model('Feed', FeedSchema);

