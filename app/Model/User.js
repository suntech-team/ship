var moment = require('moment');
var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    fullname: String,
    email: String,
    password: String,
    phone: String,
    address: String,
    birthday: Date,
    age: Number,
    gender: String,
    access_token: String,
    user_type: String,

    facebook_id: String,
    facebook_name: String,
    facebook_email: String,
    facebook_phone: String,
    facebook_avatar: String,
    facebook_token: String,

    created_at: {type: Date, default: Date.now},
    updated_at: { type: Date, default: Date.now }
}, {id: false, versionKey: 'v'});

module.exports = mongoose.model('User', UserSchema);
