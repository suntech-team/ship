var moment = require('moment');
var cache = require('memory-cache');
var request = require('request');
var async = require('async');
var axios = require('axios');
var Knwl = require("knwl.js");
var _ = require('lodash');
var groupArray = require('group-array');

var Helper = require('../Helper/Helper');
var Config = require('../Config/Config');

var knwlInstance = new Knwl('english');

knwlInstance.register('phones', Helper.Phones);

function getInitFeeds() {

    cache.put('feedsTime', moment().unix());

    async.map(Config.Fb_Groups_List, feedResult, (err, feedsArr) => {
        let feedsArrLeft = feedsArr.length;
        let feedLeft = 0;
        let newFeedsArr = [];

        _.each(feedsArr, (feeds) => {
            feedsArrLeft--;

            let feedLeft = feeds.data ? feeds.data.length : 0;

            if (feeds.data.length > 0) {
                _.each(feeds.data, (feed) => {
                    if (feed.message && feed.message.length <= 250 && feed.message.length >= 0 && !feed.picture) {
                        var message = _.toString(feed.message);
                        knwlInstance.init(message);

                        let phoneFound = knwlInstance.get('phones');

                        if (phoneFound.length > 0) {
                            let phone = phoneFound[0]['phone'].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s/g]/gi, '');
                            feed.phone = phone;
                        }

                        delete feed.comments;
                        delete feed.picture;
                        
                        newFeedsArr.push(feed);
                    }

                    feedLeft--;
                })
            }
        })

        if(feedsArrLeft === 0 && feedLeft === 0 && feedLeft != undefined) {
            // newFeedsArr.login = cache.get('member_login_state');

            let newSortArr = sortByDateTime(newFeedsArr, { prop: "created_time" });
            checkFeedExistAndSave(newFeedsArr)
        }
    })
}

function createCache() {
    NewFeeds.find({}, (err, feeds) => {
        if(feeds.length >= 1000) {
            removeFeeds()
        }

        if(err) return console.log(err);
        let newSortArr = sortByDateTime(feeds, { prop: "created_time" });
        cache.put('feeds', newSortArr)
    })
}

function feedResult(group, callback) {
    let since = moment().unix() - 8;

    let FB_API_URI = `${Config.facebook.version_url}/${group.id}/feed${Config.Fb_Query_Params}&since=${since}&access_token=${Config.user_access_token}`

    request(FB_API_URI, (error, response, body) => {

        if (error) {
            return callback(error, null);
        }

        if (!error && response.statusCode == 200) {
            callback(null, JSON.parse(body));
        }
    })
}


function checkFeedExistAndSave(feeds) {

    let feedsLeft = feeds.length;

    let newFeeds = [];

    _.each(feeds, (feed) => {
        Feed.findOne({id: feed.id}, (err, data) => {

            let time = moment(feed['updated_time']).unix() - moment(feed['created_time']).unix();

            if (_.isNull(data) && time === 0) {
                newFeeds.push(feed);
                Feed.create(feed);
                console.log(feed);
            }

            feedsLeft--;

            if (feedsLeft === 0) {
                cache.put('feed', newFeeds);
            }
        })
    });
}

var sortByDateTime = (function () {

    var _toString = Object.prototype.toString,

        _parser = function (x) { return x; },

        _getItem = function (x) {
            return this.parser((x !== null && typeof x === "object" && x[this.prop]) || x);
        };
    return function (array, o) {
        if (!(array instanceof Array) || !array.length)
            return [];
        if (_toString.call(o) !== "[object Object]")
            o = {};
        if (typeof o.parser !== "function")
            o.parser = _parser;
        o.desc = !!o.desc ? -1 : 1;
        return array.sort(function (a, b) {
            a = _getItem.call(o, a);
            b = _getItem.call(o, b);
            return o.desc * (a > b ? -1 : +(a < b));
        });
    };
}());

function getFeedsFromCache() {
    let feeds = cache.get('feeds');

    console.log(feeds);
}

setInterval(getInitFeeds, 2000);

// setInterval(getFeedsFromCache, 3000);
