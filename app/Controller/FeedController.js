var cache = require('memory-cache');

exports.fetchFeeds = function(req, res) {
    
    let feeds = cache.get('feeds');

   	return res.json({
   	 	status: 200,
        code: 200,
        error: false,
        feeds: feeds
    })
}

exports.postFeed = function(req, res) {
	
}
